import sys
from processors import *


#######################################
#   Scene Template:
#######################################
class Scene:
    """A simple Scene template"""
    def __init__(self):
        self.batch = None
        self.world = None
        self._set_scene = None
        self.player = None
        self.soundtrack = None

    def update(self, dt):
        raise NotImplementedError

    def next_scene(self, scene_class_name, map_name=None):
        # A hack to allow setting the next scene by name:
        try:
            self.soundtrack.delete()
        except AttributeError:
            pass
        thismodule = sys.modules[__name__]
        scene_class = getattr(thismodule, scene_class_name)
        self._set_scene(scene_class, map_name=map_name)


#######################################
#   The Scene definitions:
#######################################
class TitleScene(Scene):
    def __init__(self, window):
        super().__init__()
        self.window = window
        self.world = esper.CachedWorld()
        self.batch = pyglet.graphics.Batch()
        self.foreground = pyglet.graphics.OrderedGroup(2)
        self.midsection = pyglet.graphics.OrderedGroup(1)
        self.background = pyglet.graphics.OrderedGroup(0)
        self.groups = self.background, self.midsection, self.foreground
        self.map_size = 480, 270
        self.soundtrack = pyglet.resource.media("guns.wav").play()

        self.world.add_processor(RenderProcessor(window=window, batch=self.batch, resolution=self.map_size))
        self.world.add_processor(ScaleProcessor())
        self.world.add_processor(LifeProcessor(scene_setter=self.next_scene))

        title_screen_sprite = pyglet.sprite.Sprite(pyglet.resource.image("titlescreen.png"),
                                                   x=self.map_size[0]/2, y=self.map_size[1]/2,
                                                   batch=self.batch, group=self.background)
        title_screen_sprite.image.anchor_x = title_screen_sprite.width // 2
        title_screen_sprite.image.anchor_y = title_screen_sprite.height // 2
        title_screen_sprite.scale = 0

        title_screen_ent = self.world.create_entity()
        self.world.add_component(title_screen_ent, Renderable(sprite=title_screen_sprite))
        self.world.add_component(title_screen_ent, Scaling(speed=0.01, one_shot=True))

        instructions_sprite = pyglet.sprite.Sprite(pyglet.resource.image("instructions.png"), x=3,
                                                   batch=self.batch, group=self.foreground)
        instructions_sprite.opacity = 0
        self.instructions_ent = self.world.create_entity()
        self.world.add_component(self.instructions_ent, Renderable(sprite=instructions_sprite))

        @self.window.event
        def on_key_press(*unused):
            if self.world.component_for_entity(title_screen_ent, Renderable).sprite.scale > 0.5:
                self.next_scene(scene_class_name="DungeonScene", map_name="prison.tmx")

    def update(self, dt):
        self.world.process(dt)
        if self.world.component_for_entity(self.instructions_ent, Renderable).sprite.opacity < 255:
            self.world.component_for_entity(self.instructions_ent, Renderable).sprite.opacity += 0.5


class GameOverScene(Scene):
    def __init__(self, window):
        super().__init__()
        self.window = window
        self.world = esper.CachedWorld()
        self.batch = pyglet.graphics.Batch()
        self.foreground = pyglet.graphics.OrderedGroup(2)
        self.midsection = pyglet.graphics.OrderedGroup(1)
        self.background = pyglet.graphics.OrderedGroup(0)
        self.groups = self.background, self.midsection, self.foreground
        self.map_size = 480, 270

        self.world.add_processor(RenderProcessor(window=window, batch=self.batch, resolution=self.map_size))
        self.world.add_processor(ScaleProcessor())

        title_screen_sprite = pyglet.sprite.Sprite(pyglet.resource.image("gameover.png"),
                                                   x=self.map_size[0]/2, y=self.map_size[1]/2,
                                                   batch=self.batch, group=self.background)
        title_screen_sprite.image.anchor_x = title_screen_sprite.width // 2
        title_screen_sprite.image.anchor_y = title_screen_sprite.height // 2
        title_screen_sprite.scale = 20

        title_screen_ent = self.world.create_entity()
        self.world.add_component(title_screen_ent, Renderable(sprite=title_screen_sprite))
        self.world.add_component(title_screen_ent, Scaling(speed=0.5, one_shot=True))

        @self.window.event
        def on_key_press(*unused):
            if self.world.component_for_entity(title_screen_ent, Renderable).sprite.scale == 1:
                self.next_scene(scene_class_name="TitleScene")

    def update(self, dt):
        self.world.process(dt)


class DungeonScene(Scene):
    def __init__(self, window):
        super().__init__()
        self.window = window
        self.world = esper.CachedWorld()
        self.batch = pyglet.graphics.Batch()
        self.foreground = pyglet.graphics.OrderedGroup(2)
        self.midsection = pyglet.graphics.OrderedGroup(1)
        self.background = pyglet.graphics.OrderedGroup(0)
        self.groups = self.background, self.midsection, self.foreground
        self.map_size = 480, 270
        self.soundtrack = pyglet.resource.media("elf.wav").play()
        self.soundtrack.eos_action = pyglet.media.SourceGroup.loop

        self.world.add_processor(RenderProcessor(window=window, batch=self.batch, resolution=self.map_size))
        self.world.add_processor(ScaleProcessor())
        self.world.add_processor(MovementProcessor())
        self.world.add_processor(CommandProcessor())
        self.world.add_processor(CameraProcessor(offset_x=-self.map_size[0]//2, offset_y=-self.map_size[1]//2))
        self.world.add_processor(CollisionProcessor(region=(-16, -16, self.map_size[0]+16, self.map_size[1]+16)), priority=9)
        self.world.add_processor(AnimationProcessor())
        self.world.add_processor(AIProcessor())
        self.world.add_processor(LifeProcessor(scene_setter=self.next_scene))
        self.world.add_processor(FlashProcessor())
        self.world.add_processor(SceneChangeProcessor(scene_setter=self.next_scene))
        self.world.add_processor(HudProcessor())

        @self.window.event
        def on_key_press(key, mod):
            if key == pyglet.window.key.UP:
                self.world.component_for_entity(self.player, Command).move_up = True
            if key == pyglet.window.key.DOWN:
                self.world.component_for_entity(self.player, Command).move_down = True
            if key == pyglet.window.key.LEFT:
                self.world.component_for_entity(self.player, Command).move_left = True
            if key == pyglet.window.key.RIGHT:
                self.world.component_for_entity(self.player, Command).move_right = True

        @self.window.event
        def on_key_release(key, mod):
            if key == pyglet.window.key.UP:
                self.world.component_for_entity(self.player, Command).move_up = False
            if key == pyglet.window.key.DOWN:
                self.world.component_for_entity(self.player, Command).move_down = False
            if key == pyglet.window.key.LEFT:
                self.world.component_for_entity(self.player, Command).move_left = False
            if key == pyglet.window.key.RIGHT:
                self.world.component_for_entity(self.player, Command).move_right = False

    def update(self, dt):
        self.world.process(dt)


class WinnerScene(Scene):
    def __init__(self, window):
        super().__init__()
        self.window = window
        self.world = esper.CachedWorld()
        self.batch = pyglet.graphics.Batch()
        self.foreground = pyglet.graphics.OrderedGroup(2)
        self.midsection = pyglet.graphics.OrderedGroup(1)
        self.background = pyglet.graphics.OrderedGroup(0)
        self.groups = self.background, self.midsection, self.foreground
        self.map_size = 480, 270

        self.world.add_processor(RenderProcessor(window=window, batch=self.batch, resolution=self.map_size))

        self.title_screen_sprite = pyglet.sprite.Sprite(pyglet.resource.image("winner.png"),
                                                        batch=self.batch, group=self.background)
        self.title_screen_sprite.y = -500

        title_screen_ent = self.world.create_entity()
        self.world.add_component(title_screen_ent, Renderable(sprite=self.title_screen_sprite))

        @self.window.event
        def on_key_press(*unused):
            if self.world.component_for_entity(title_screen_ent, Renderable).sprite.y == 0:
                self.next_scene(scene_class_name="TitleScene")

    def update(self, dt):
        if self.title_screen_sprite.y < 0:
            self.title_screen_sprite.y += 0.5
        self.world.process(dt)
