#!/usr/bin/env python3
from scenes import *
from utilities.scenemanager import SceneManager


FPS = 60
WIDTH = 960
HEIGHT = 540

pyglet.options['audio'] = ('directsound', 'openal', 'pulse', 'silent')
window = pyglet.window.Window(width=WIDTH, height=HEIGHT, resizable=True, caption="Ripple")
if len(sys.argv) > 1:
    if sys.argv[1] == "--fullscreen":
        window.set_fullscreen()

scene_manager = SceneManager(window=window)
scene_manager.set_scene(scene_class=TitleScene)


if __name__ == "__main__":
    pyglet.clock.schedule_interval(scene_manager.update, 1/FPS)
    pyglet.app.run()
    sys.exit()
