

class QuadTree:
    def __init__(self, bodies, region, depth=8):
        """A Quad Tree implementation for static body collision detection.

        :param bodies: A list of AABB tuples.
        :param region: The region in which collisions will be checked.
        :param depth: The current depth recursion. Maximum depth can be chosen.
        """
        self.nw = self.ne = self.se = self.sw = None

        # If we've reached the maximum depth then insert all items into this quadrant.
        depth -= 1
        if depth == 0 or not bodies:
            self.body_list = bodies
            return

        region_left, region_top, region_right, region_bottom = region
        self.center_x = (region_left + region_right) // 2
        self.center_y = (region_bottom + region_top) // 2
        self.depth = depth
        self.body_list = []

        self.add(bodies=bodies, region=region)

    def add(self, bodies, region):
        region_left, region_top, region_right, region_bottom = region
        center_x = self.center_x
        center_y = self.center_y
        depth = self.depth

        nw_items = []
        ne_items = []
        se_items = []
        sw_items = []

        for body in bodies:
            left, top, right, bottom = body.aabb

            in_nw = left <= center_x and bottom >= center_y
            in_sw = left <= center_x and top <= center_y
            in_ne = right >= center_x and bottom >= center_y
            in_se = right >= center_x and top <= center_y

            # If it overlaps all 4 quadrants then insert it at the current
            # depth, otherwise append it to a list to be inserted under every
            # quadrant that it overlaps.
            if in_nw and in_ne and in_se and in_sw:
                self.body_list.append(body)
            else:
                if in_nw:
                    nw_items.append(body)
                if in_ne:
                    ne_items.append(body)
                if in_se:
                    se_items.append(body)
                if in_sw:
                    sw_items.append(body)

        # Create the sub-quadrants, recursively.
        if nw_items:
            self.nw = QuadTree(nw_items, (region_left, region_top, center_x, center_y), depth)
        if ne_items:
            self.ne = QuadTree(ne_items, (center_x, region_top, region_right, center_y), depth)
        if se_items:
            self.se = QuadTree(se_items, (center_x, center_y, region_right, region_bottom), depth)
        if sw_items:
            self.sw = QuadTree(sw_items, (region_left, center_y, center_x, region_bottom), depth)

    def get_hits(self, aabb):
        aleft, atop, aright, abottom = aabb

        def simple_overlap(body_aabb):
            bleft, btop, bright, bbottom = body_aabb
            # An overlap has occured if ALL of these are True, otherwise return False:
            return bleft < aright and bright > aleft and btop < abottom and bbottom > atop

        # First check hits at the current level
        hits = set(body for body in self.body_list if simple_overlap(body.aabb))

        # Then recursively check the lower quadrants.
        if self.nw and aleft <= self.center_x and abottom >= self.center_y:
            hits |= self.nw.get_hits(aabb)
        if self.sw and aleft <= self.center_x and atop <= self.center_y:
            hits |= self.sw.get_hits(aabb)
        if self.ne and aright >= self.center_x and abottom >= self.center_y:
            hits |= self.ne.get_hits(aabb)
        if self.se and aright >= self.center_x and atop <= self.center_y:
            hits |= self.se.get_hits(aabb)

        return hits
