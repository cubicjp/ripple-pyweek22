import os
import zlib
import base64
import struct
from xml.etree import ElementTree

import pyglet


def _get_map_root(tmx_filename):
    map_root = ElementTree.parse(tmx_filename).getroot()
    orientation = map_root.get("orientation")
    render_order = map_root.get("renderorder")
    assert render_order == "right-down", "Only right-down render order is supported"
    assert orientation == "orthogonal", "Only orthogonal orientation is supported"
    return map_root


def _decode_layer(layer):
    layer_name = layer.get("name")

    data_element = layer.find("data")
    # data = data_element
    if data_element.get("encoding") == "base64":
        data = base64.b64decode(data_element.text.strip())
        if data_element.get("compression") == "zlib":
            data = zlib.decompress(data)
        gid_tuples = [struct.unpack("<I", data[i:i + 4]) for i in range(0, len(data), 4)]
        gid_list = [g[0] for g in gid_tuples]
    elif data_element.get("encoding") == "csv":
        gid_list = [int(g) for g in data_element.text.replace('\n', '').split(',')]
    else:
        raise NotImplementedError

    layer_dict = dict(layer_name=layer_name, gids=gid_list)

    return layer_dict


def _decode_tileset(tileset):
    # TODO: support spacing and margin
    tileset_name = tileset.get("name")
    first_gid = int(tileset.get("firstgid"))
    columns = int(tileset.get("columns"))
    tile_width = int(tileset.get("tilewidth"))
    tile_height = int(tileset.get("tileheight"))
    tile_count = int(tileset.get("tilecount"))
    image = tileset.find("image")
    image_filename = image.get("source")
    image_width = image.get("width")
    image_height = image.get("height")
    tileset_dict = dict(tileset_name=tileset_name,
                        image_filename=image_filename,
                        image_width=image_width,
                        image_height=image_height)
    for i in range(tile_count):
        column = i % columns
        row = (i % tile_count - column) // columns
        x = column * tile_width
        y = row * tile_height
        gid = i + first_gid
        tileset_dict[gid] = (x, y)
    return tileset_dict


def _get_map_gid_locations(map_root):
    # TODO: support different render order
    tile_width = int(map_root.get("tilewidth"))
    tile_height = int(map_root.get("tileheight"))
    image_width = int(map_root.get("width")) * tile_width
    image_height = int(map_root.get("height")) * tile_height
    map_columns = image_width // tile_width
    map_rows = image_height // tile_height
    tile_count = map_columns * map_rows
    gid_location_dict = dict()
    for i in range(tile_count):
        column = i % map_columns
        row = (i % tile_count - column) // map_columns
        x = column * tile_width
        y = row * tile_height
        gid = i + 1
        gid_location_dict[gid] = (x, y)
    return gid_location_dict


def _map_copy_rects(map_root, layer):

    all_tilesets = map_root.findall("tileset")
    map_gid_locations = _get_map_gid_locations(map_root)
    layer_dict = _decode_layer(layer)

    for i, gid in enumerate(layer_dict["gids"]):
        if gid == 0:
            continue
        destination_x, destination_y = map_gid_locations[i + 1]
        tileset_dict = [_decode_tileset(ts) for ts in all_tilesets
                        if gid in _decode_tileset(ts)][0]
        source_x, source_y = tileset_dict[gid]
        image_file_name = tileset_dict["image_filename"]

        yield image_file_name, source_x, source_y, destination_x, destination_y


def create_pyglet_textures(tmx_filename):
    map_root = _get_map_root(tmx_filename)
    file_dir_name = os.path.dirname(tmx_filename)
    tile_width = int(map_root.get("tilewidth"))
    tile_height = int(map_root.get("tileheight"))
    map_width = int(map_root.get("width")) * tile_width
    map_height = int(map_root.get("height")) * tile_height
    all_layers = map_root.findall("layer")

    texture_list = []

    for layer in all_layers:

        texture = pyglet.image.Texture.create(width=map_width, height=map_height, rectangle=True)

        for image_name, source_x, source_y, dest_x, dest_y in _map_copy_rects(map_root, layer):
            image_file_path = os.path.join(file_dir_name, image_name)
            # TODO: don't continuously reload the image for each tile:
            image = pyglet.image.load(image_file_path)

            # Flip y render order for pyglet:
            dest_y = map_height - tile_height - dest_y
            source_y = image.height - tile_height - source_y

            source_image_rect = image.get_region(source_x, source_y, tile_width, tile_height)
            texture.blit_into(source=source_image_rect, x=dest_x, y=dest_y, z=0)

        texture_list.append(texture)

    return texture_list


def get_map_size(tmx_filename):
    map_root = _get_map_root(tmx_filename)
    tile_width = int(map_root.get("tilewidth"))
    tile_height = int(map_root.get("tileheight"))
    map_width = int(map_root.get("width")) * tile_width
    map_height = int(map_root.get("height")) * tile_height

    return map_width, map_height


def get_sprite_lists(tmx_filename, flip_y=True):
    map_root = _get_map_root(tmx_filename)
    tile_width = int(map_root.get("tilewidth"))
    tile_height = int(map_root.get("tileheight"))
    map_height = int(map_root.get("height")) * tile_height
    all_layers = map_root.findall("layer")

    rect_lists = []

    for layer in all_layers:

        rects = []

        for image_name, source_x, source_y, dest_x, dest_y in _map_copy_rects(map_root, layer):
            # Rely on pyglet's resource module to provide the image:
            image = pyglet.resource.image(image_name)

            # Flip y render order for pyglet:
            if flip_y:
                dest_y = map_height - tile_height - dest_y
                source_y = image.height - tile_height - source_y

            rects.append(dict(image_name=image_name,
                              width=tile_width,
                              height=tile_height,
                              source_x=source_x,
                              source_y=source_y,
                              dest_x=dest_x,
                              dest_y=dest_y))

        rect_lists.append(rects)

    return rect_lists


def get_object_layer(tmx_filename, layer_name, flip_y=True):
    map_root = _get_map_root(tmx_filename)
    tile_height = int(map_root.get("tileheight"))
    map_height = int(map_root.get("height")) * tile_height

    for object_group in map_root.findall("objectgroup"):
        if object_group.get("name") == layer_name:
            object_list = []
            for obj in object_group:
                width = height = 0
                name = obj_type = arg = ""
                if obj.get("width"):
                    width = float(obj.get("width"))
                if obj.get("height"):
                    height = float(obj.get("height"))
                if obj.get("type"):
                    obj_type = str(obj.get("type"))
                if obj.get("arg"):
                    arg = str(obj.get("arg"))
                if obj.get("name"):
                    name = str(obj.get("name"))

                property_list = []
                if obj.find("properties"):
                    for prop in obj.find("properties"):
                        property_list.append(dict(name=prop.get("name"), value=prop.get("value")))

                x = float(obj.get("x"))
                y = float(obj.get("y"))
                if flip_y:
                    y = map_height - height - y
                object_list.append(dict(name=name, x=x, y=y, width=width, height=height,
                                        type=obj_type, arg=arg, properties=property_list))

            return object_list
