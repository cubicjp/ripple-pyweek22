import random
import utilities.tiledloader as tiledloader

from components import *


class SceneManager:
    """A simple scene manager"""
    def __init__(self, window):
        self.current_scene = None
        self.window = window

    def set_scene(self, scene_class, map_name=None):
        scene = scene_class(window=self.window)
        if map_name:
            tmx_file = pyglet.resource.file(map_name).name
            player_ent = create_map(tmx_file=tmx_file, world=scene.world, batch=scene.batch)
            scene.player = player_ent
        self.current_scene = scene
        self.current_scene._set_scene = self.set_scene

    def update(self, dt):
        self.current_scene.update(dt)


###########################################
#   Populate scenes from tmx files:
###########################################
def create_map(tmx_file, world, batch):
    # Create tile sprites, layer by layer:
    for i, layer in enumerate(tiledloader.get_sprite_lists(tmx_filename=tmx_file)):
        for sprite_dict in layer:

            source_x = sprite_dict['source_x']
            source_y = sprite_dict['source_y']
            tile_width = sprite_dict['width']
            tile_height = sprite_dict['height']
            sprite_sheet = pyglet.resource.image(sprite_dict["image_name"])
            tile_image = sprite_sheet.get_region(source_x, source_y, tile_width, tile_height)
            sprite = pyglet.sprite.Sprite(img=tile_image, x=sprite_dict["dest_x"], y=sprite_dict["dest_y"],
                                          z=i, batch=batch)
            ent = world.create_entity()
            world.add_component(ent, Renderable(sprite=sprite))
            print(i)

    # Create static bodies for collions:
    for obj in tiledloader.get_object_layer(tmx_filename=tmx_file, layer_name="collisions"):
        body_aabb = obj['x'], obj['y'], obj['x'] + obj['width'], obj['y'] + obj['height']
        body_ent = world.create_entity()
        world.add_component(body_ent, StaticBody(aabb=body_aabb))

    for obj in tiledloader.get_object_layer(tmx_filename=tmx_file, layer_name="triggers"):
        trigger_aabb = obj['x'], obj['y'], obj['x'] + obj['width'], obj['y'] + obj['height']
        for property_dict in obj['properties']:
            trig_ent = world.create_entity()
            world.add_component(trig_ent, StaticBody(trigger_aabb, body_type="trigger", target=property_dict["value"]))

    player_x = player_y = 0
    for obj in tiledloader.get_object_layer(tmx_filename=tmx_file, layer_name="entities"):
        if obj['type'] == "player":
            player_x = obj['x']
            player_y = obj['y']
        elif obj['type'] == "spider":
            create_spider(world=world, batch=batch, x=obj['x'], y=obj['y'], z=5)
        elif obj['type'] == "skeleton":
            create_skeleton(world=world, batch=batch, x=obj['x'], y=obj['y'], z=5)
        elif obj['type'] == "ghost":
            create_ghost(world=world, batch=batch, x=obj['x'], y=obj['y'], z=5)

    return create_player(world=world, batch=batch, x=player_x, y=player_y, z=6)


##########################################
#   Templates for creating entities:
##########################################
def create_player(world, batch, x=0, y=0, z=0):

    grid = pyglet.image.ImageGrid(pyglet.resource.image("characters.png"), rows=8, columns=12)

    anim_idle_up = grid[52]
    anim_idle_down = grid[88]
    anim_idle_left = grid[76]
    anim_idle_right = grid[64]
    anim_right = pyglet.image.Animation.from_image_sequence((grid[63], grid[64], grid[65], grid[64]), period=0.2)
    anim_left = pyglet.image.Animation.from_image_sequence((grid[75], grid[76], grid[77], grid[76]), period=0.2)
    anim_up = pyglet.image.Animation.from_image_sequence((grid[51], grid[52], grid[53], grid[52]), period=0.2)
    anim_down = pyglet.image.Animation.from_image_sequence((grid[87], grid[88], grid[89], grid[88]), period=0.2)

    player_sprite = pyglet.sprite.Sprite(img=anim_idle_down, batch=batch)
    player_sprite.update(x, y, z)

    player_ent = world.create_entity()
    world.add_component(player_ent, Player())
    world.add_component(player_ent, Camera())
    world.add_component(player_ent, Command())
    world.add_component(player_ent, Velocity())
    world.add_component(player_ent, Life(hp=6))
    world.add_component(player_ent, Renderable(sprite=player_sprite, aabb_x_trim=4, y_top_trim=2))
    world.add_component(player_ent, Animation(iup=anim_idle_up, idown=anim_idle_down,
                                              ileft=anim_idle_left, iright=anim_idle_right,
                                              mup=anim_up, mdown=anim_down,
                                              mleft=anim_left, mright=anim_right, dead=None))
    world.add_component(player_ent, SFX(hit="player_hit.wav"))

    create_hud(world=world, batch=batch)

    return player_ent


def create_hud(world, batch):
    hud_grid = pyglet.image.ImageGrid(image=pyglet.resource.image("meter.png"), rows=6, columns=1)
    hud_sprite = pyglet.sprite.Sprite(img=hud_grid[0], batch=batch, z=7)
    hud_ent = world.create_entity()
    world.add_component(hud_ent, Hud())
    world.add_component(hud_ent, Renderable(sprite=hud_sprite))


def create_spider(world, batch, x=0, y=0, z=0):
    grid = pyglet.image.ImageGrid(pyglet.resource.image("characters.png"), rows=8, columns=12)

    anim_idle_up = grid[10]
    anim_idle_right = grid[22]
    anim_idle_left = grid[34]
    anim_idle_down = grid[46]
    anim_up = pyglet.image.Animation.from_image_sequence((grid[9], grid[10], grid[11], grid[10]), period=0.2)
    anim_right = pyglet.image.Animation.from_image_sequence((grid[21], grid[22], grid[23], grid[22]), period=0.2)
    anim_left = pyglet.image.Animation.from_image_sequence((grid[33], grid[34], grid[35], grid[34]), period=0.2)
    anim_down = pyglet.image.Animation.from_image_sequence((grid[45], grid[46], grid[47], grid[46]), period=0.2)

    spider_sprite = pyglet.sprite.Sprite(img=anim_idle_down, batch=batch)
    spider_sprite.update(x, y, z)

    monster_ent = world.create_entity()
    world.add_component(monster_ent, Monster(enemy_type="spider"))
    world.add_component(monster_ent, Command())
    world.add_component(monster_ent, Velocity(accel=0.01, terminal=random.uniform(0.3, 0.8)))
    world.add_component(monster_ent, Renderable(sprite=spider_sprite, aabb_x_trim=4, aabb_y_trim=4))
    world.add_component(monster_ent, Animation(iup=anim_idle_up, idown=anim_idle_down,
                                               ileft=anim_idle_left, iright=anim_idle_right,
                                               mup=anim_up, mdown=anim_down,
                                               mleft=anim_left, mright=anim_right))
    world.add_component(monster_ent, Life(hp=4))
    world.add_component(monster_ent, SFX())
    world.add_component(monster_ent, Brain())


def create_skeleton(world, batch, x=0, y=0, z=0):
    grid = pyglet.image.ImageGrid(pyglet.resource.image("characters.png"), rows=8, columns=12)

    anim_idle_up = grid[58]
    anim_idle_right = grid[70]
    anim_idle_left = grid[82]
    anim_idle_down = grid[94]
    anim_up = pyglet.image.Animation.from_image_sequence((grid[57], grid[58], grid[59], grid[58]), period=0.2)
    anim_right = pyglet.image.Animation.from_image_sequence((grid[69], grid[70], grid[71], grid[71]), period=0.2)
    anim_left = pyglet.image.Animation.from_image_sequence((grid[81], grid[82], grid[83], grid[82]), period=0.2)
    anim_down = pyglet.image.Animation.from_image_sequence((grid[93], grid[94], grid[95], grid[94]), period=0.2)

    spider_sprite = pyglet.sprite.Sprite(img=anim_idle_down, batch=batch)
    spider_sprite.update(x, y, z)

    monster_ent = world.create_entity()
    world.add_component(monster_ent, Monster(enemy_type="skeleton"))
    world.add_component(monster_ent, Command())
    world.add_component(monster_ent, Velocity(accel=0.03, terminal=random.uniform(0.5, 1.1)))
    world.add_component(monster_ent, Renderable(sprite=spider_sprite, aabb_x_trim=4, aabb_y_trim=4))
    world.add_component(monster_ent, Animation(iup=anim_idle_up, idown=anim_idle_down,
                                               ileft=anim_idle_left, iright=anim_idle_right,
                                               mup=anim_up, mdown=anim_down,
                                               mleft=anim_left, mright=anim_right))
    world.add_component(monster_ent, Life(hp=6))
    world.add_component(monster_ent, SFX())
    world.add_component(monster_ent, Brain(cooldown=25))


def create_ghost(world, batch, x=0, y=0, z=0):
    grid = pyglet.image.ImageGrid(pyglet.resource.image("characters.png"), rows=8, columns=12)

    anim_idle_up = pyglet.image.Animation.from_image_sequence((grid[6], grid[7], grid[8], grid[7]), period=0.4)
    anim_idle_right = pyglet.image.Animation.from_image_sequence((grid[18], grid[19], grid[20], grid[19]), period=0.4)
    anim_idle_left = pyglet.image.Animation.from_image_sequence((grid[30], grid[31], grid[32], grid[31]), period=0.4)
    anim_idle_down = pyglet.image.Animation.from_image_sequence((grid[42], grid[43], grid[44], grid[43]), period=0.4)
    anim_up = pyglet.image.Animation.from_image_sequence((grid[6], grid[7], grid[8], grid[7]), period=0.2)
    anim_right = pyglet.image.Animation.from_image_sequence((grid[18], grid[19], grid[20], grid[19]), period=0.2)
    anim_left = pyglet.image.Animation.from_image_sequence((grid[30], grid[31], grid[32], grid[31]), period=0.2)
    anim_down = pyglet.image.Animation.from_image_sequence((grid[42], grid[43], grid[44], grid[43]), period=0.2)

    spider_sprite = pyglet.sprite.Sprite(img=anim_idle_down, batch=batch)
    spider_sprite.update(x, y, z)

    monster_ent = world.create_entity()
    world.add_component(monster_ent, Monster(enemy_type="ghost"))
    world.add_component(monster_ent, Command())
    world.add_component(monster_ent, Velocity(accel=0.01, terminal=0.3))
    world.add_component(monster_ent, Renderable(sprite=spider_sprite, aabb_x_trim=4, aabb_y_trim=4))
    world.add_component(monster_ent, Animation(iup=anim_idle_up, idown=anim_idle_down,
                                               ileft=anim_idle_left, iright=anim_idle_right,
                                               mup=anim_up, mdown=anim_down,
                                               mleft=anim_left, mright=anim_right))
    world.add_component(monster_ent, Life(hp=2))
    world.add_component(monster_ent, SFX())
    world.add_component(monster_ent, Brain(cooldown=300, counter=0))
