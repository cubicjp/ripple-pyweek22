from pyglet.gl import *


class FixedResolutionViewport:
    def __init__(self, target_window, resolution, smoothed=False):
        """Create a fixed resolution viewport that maintains aspect ratio.

        Create a fixed resolution viewport that maintains aspect ratio, even
        when the Window is resized. Pillars or Letterboxes will appear
        as necessary.
        :param target_window: The pyglet Window instance.
        :param resolution: The target resolution as a tuple (x, y)
        :param smoothed: Whether or not you want bilinear smoothing.
        """
        self.window = target_window
        self.fixed_width = resolution[0]
        self.fixed_height = resolution[1]
        self.scale_width = target_window.width
        self.scale_height = target_window.height
        self.target_x = 0
        self.target_y = 0

        self.texture = pyglet.image.Texture.create(self.fixed_width, self.fixed_height)

        if not smoothed:
            glTexParameteri(self.texture.target, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
            glTexParameteri(self.texture.target, GL_TEXTURE_MIN_FILTER, GL_NEAREST)

        @target_window.event
        def on_resize(new_width, new_height):
            """Recalculate the final scaling when the window is resized"""
            fixed_width = self.fixed_width
            fixed_height = self.fixed_height

            aspect_width = new_width / fixed_width
            aspect_height = new_height / fixed_height

            # Create letterbox/pillar effect when resizing window:
            if aspect_width > aspect_height:
                self.scale_width = aspect_height * fixed_width
                self.scale_height = aspect_height * fixed_height
            else:
                self.scale_width = aspect_width * fixed_width
                self.scale_height = aspect_width * fixed_height
            self.target_x = (new_width - self.scale_width) / 2
            self.target_y = (new_height - self.scale_height) / 2

    def __enter__(self):
        glViewport(0, 0, self.fixed_width, self.fixed_height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0, self.fixed_width, 0, self.fixed_height, -255, 255)
        glMatrixMode(GL_MODELVIEW)

    @staticmethod
    def set_camera(x=0, y=0, z=0):
        """Offset the camera before blitting scrolling things."""
        glLoadIdentity()
        # Clamp to integers to prevent sprite artifacts:
        glTranslatef(int(-x), int(-y), int(-z))

    @staticmethod
    def reset_camera():
        """Reset the camera to zero before blitting static things (hud)."""
        glLoadIdentity()

    def __exit__(self, *unused_args):
        """Blit the frambuffer to the Window.

        Aspect ratio is preserved, and the final image will be letterboxed
        or pillared, as appropriate. The Window can be resized freely.
        """
        window = self.window
        buffer = pyglet.image.get_buffer_manager().get_color_buffer()
        self.texture.blit_into(buffer, 0, 0, 0)

        glViewport(0, 0, window.width, window.height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0, window.width, 0, window.height, -1, 1)
        glMatrixMode(GL_MODELVIEW)

        glClearColor(0, 0, 0, 1)
        glClear(GL_COLOR_BUFFER_BIT)
        glLoadIdentity()
        glColor3f(1, 1, 1)
        self.texture.blit(self.target_x, self.target_y, width=self.scale_width, height=self.scale_height)

    def begin(self):
        self.__enter__()

    def end(self):
        self.__exit__()
