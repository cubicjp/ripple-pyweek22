import pyglet


pyglet.resource.path.append("resources")
pyglet.resource.reindex()


class Player:
    pass


class Monster:
    def __init__(self, enemy_type):
        self.enemy_type = enemy_type


class Brain:
    def __init__(self, cooldown=60, counter=60):
        self.cooldown = cooldown
        self.counter = counter


class Command:
    def __init__(self):
        self.move_up = False
        self.move_down = False
        self.move_left = False
        self.move_right = False
        self.attack = False
        self.bump_up = False
        self.bump_down = False
        self.bump_left = False
        self.bump_right = False


class Renderable:
    def __init__(self, sprite, aabb_x_trim=0, aabb_y_trim=0, y_top_trim=0):
        self.sprite = sprite
        self.width = sprite.width
        self.height = sprite.height
        self.x_trim = aabb_x_trim
        self.y_trim = aabb_y_trim
        self.y_top_trim = y_top_trim

    @property
    def x(self):
        return self.sprite.x

    @x.setter
    def x(self, value):
        self.sprite.x = value

    @property
    def y(self):
        return self.sprite.y

    @y.setter
    def y(self, value):
        self.sprite.y = value

    @property
    def aabb(self):
        sprite = self.sprite
        return (sprite.x + self.x_trim,
                sprite.y + self.y_trim,
                sprite.x + self.width - self.x_trim,
                sprite.y + self.height - self.y_trim - self.y_top_trim)

    @property
    def image(self):
        return self.sprite.image

    @image.setter
    def image(self, image):
        # Setting the image resets the animation
        if self.sprite.image is not image:
            self.sprite.image = image

    def __del__(self):
        try:
            self.sprite.delete()
        except AssertionError:
            pass


class StaticBody:
    def __init__(self, aabb, body_type="barrier", target=None):
        self.aabb = aabb
        self.body_type = body_type
        self.target = target


class SceneTrigger:
    def __init__(self, target):
        self.target = target


class Velocity:
    def __init__(self, x=0, y=0, accel=0.1, decel=0.75, terminal=1.5):
        self.x = x
        self.y = y
        self.accel = accel
        self.decel = decel
        self.terminal = terminal


class Camera:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class Scaling:
    def __init__(self, speed=0.1, target=1, one_shot=False):
        self.target = target
        self.speed = speed
        self.one_shot = one_shot


class Animation:
    def __init__(self, iup, idown, ileft, iright, mup, mdown, mleft, mright, dead=None):
        self.idle_up = iup
        self.idle_down = idown
        self.idle_left = ileft
        self.idle_right = iright
        self.move_up = mup
        self.move_down = mdown
        self.move_left = mleft
        self.move_right = mright
        self.dead = dead


class SFX:
    def __init__(self, hit="hit.wav", death="death.wav"):
        self.hit = pyglet.resource.media(hit, streaming=False)
        self.death = pyglet.resource.media(death, streaming=False)


class Life:
    def __init__(self, hp=100, lifespan=None):
        self.hp = hp
        self.lifespan = lifespan
        self.dead = False


class Flash:
    def __init__(self):
        self.color = (255, 0, 0)
        self.duration = 10
        self.frequency = 2
        self.count = 0


class Hud:
    pass


