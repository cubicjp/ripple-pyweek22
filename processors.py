import math
import time
import random
import esper

from components import *
from utilities.viewport import FixedResolutionViewport
from utilities.quadtree import QuadTree


class RenderProcessor(esper.Processor):
    def __init__(self, window, batch, resolution):
        super().__init__()
        self.batch = batch
        self.window = window
        self.window.clear()
        self.viewport = FixedResolutionViewport(target_window=window, resolution=resolution)

    def process(self, dt):
        with self.viewport:
            self.window.clear()
            try:
                cam = [cam for ent, cam in self.world.get_component(Camera)][0]  # There should only be one anyway.
                self.viewport.set_camera(cam.x, cam.y)
            except IndexError:
                pass     # No Camera component is instantiated
            self.batch.draw()


class MovementProcessor(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, dt):
        for ent, (rend, vel) in self.world.get_components(Renderable, Velocity):
            rend.x += vel.x
            rend.y += vel.y


class ScaleProcessor(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, dt):
        for ent, (rend, scale) in self.world.get_components(Renderable, Scaling):
            if rend.sprite.scale == scale.target:
                if scale.one_shot:
                    self.world.remove_component(entity=ent, component_type=Scaling)
                continue
            elif rend.sprite.scale < scale.target:
                rend.sprite.scale += scale.speed
                if rend.sprite.scale > scale.target:
                    rend.sprite.scale = scale.target
            elif rend.sprite.scale > scale.target:
                rend.sprite.scale -= scale.speed
                if rend.sprite.scale < scale.target:
                    rend.sprite.scale = scale.target


class CommandProcessor(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, dt):
        for ent, (rend, cmd, vel) in self.world.get_components(Renderable, Command, Velocity):
            if cmd.move_up and cmd.move_down:
                vel.y *= vel.decel
            elif cmd.move_up:
                vel.y = min(vel.y + vel.accel, vel.terminal)
            elif cmd.move_down:
                vel.y = max(vel.y - vel.accel, -vel.terminal)
            else:
                vel.y *= vel.decel
            # TODO: clamp diagonal movement
            if cmd.move_left and cmd.move_right:
                vel.x *= vel.decel
            elif cmd.move_left:
                vel.x = max(vel.x - vel.accel, -vel.terminal)
            elif cmd.move_right:
                vel.x = min(vel.x + vel.accel, vel.terminal)
            else:
                vel.x *= vel.decel

            if cmd.bump_up:
                rend.y += 10
                cmd.bump_up = False
            elif cmd.bump_down:
                rend.y -= 10
                cmd.bump_down = False
            elif cmd.bump_left:
                rend.x -= 10
                cmd.bump_left = False
            elif cmd.bump_right:
                rend.x += 10
                cmd.bump_right = False


class CameraProcessor(esper.Processor):
    def __init__(self, offset_x=0, offset_y=0):
        super().__init__()
        self.offset_x = offset_x
        self.offset_y = offset_y

    def process(self, dt):
        for ent, (cam, rend) in self.world.get_components(Camera, Renderable):
            cam.x = rend.sprite.x + self.offset_x
            cam.y = rend.sprite.y + self.offset_y


class CollisionProcessor(esper.Processor):
    def __init__(self, region):
        super().__init__()
        self.region = region
        self.quadtree = None

    @staticmethod
    def _check_overlap(bodya, bodyb):
        aleft, atop, aright, abottom = bodya
        bleft, btop, bright, bbottom = bodyb
        # An overlap has occured if ALL of these are True, otherwise return False:
        return bleft < aright and bright > aleft and btop < abottom and bbottom > atop

    @staticmethod
    def _get_overlap(static_aabb, moveable_aabb):
        """Returns a tuple: ("direction", pixles)"""
        aleft, atop, aright, abottom = static_aabb
        bleft, btop, bright, bbottom = moveable_aabb
        results = [(aleft - bright, "right"),
                   (-(bbottom - atop), "top"),
                   (-(aright - bleft), "left"),
                   (btop - abottom, "bottom")]
        return sorted(results, reverse=True)[0]     # Only return the shallowest overlap

    def _damage_enemy(self, ent, life, sfx):
        life.hp -= 1
        sfx.hit.play()
        self.world.add_component(ent, Flash())

    def _damage_player(self, ent, life, sfx):
        life.hp -= 1
        sfx.hit.play()
        self.world.add_component(ent, Flash())

    def process(self, dt):
        if not self.quadtree:
            static_bodies = [b for ent, b in self.world.get_component(StaticBody)]
            self.quadtree = QuadTree(bodies=static_bodies, region=self.region)

        for ent, (rend, cmd, life, pla, sfx) in self.world.get_components(Renderable, Command, Life, Player, SFX):
            # Player hits against the enviroment
            for body in self.quadtree.get_hits(rend.aabb):
                if body.body_type == "barrier":
                    overlap = self._get_overlap(body.aabb, rend.aabb)
                    if overlap[1] == "top":
                        rend.y += overlap[0]
                    elif overlap[1] == "bottom":
                        rend.y -= overlap[0]
                    elif overlap[1] == "left":
                        rend.x -= overlap[0]
                    elif overlap[1] == "right":
                        rend.x += overlap[0]
                elif body.body_type == "trigger":
                    self.world.add_component(ent, SceneTrigger(target=body.target))

            for m_ent, (m_rend, m_cmd, m_life, mon, m_sfx) in self.world.get_components(Renderable, Command, Life, Monster, SFX):
                # Monster hits against the environment
                for body in self.quadtree.get_hits(m_rend.aabb):
                    overlap = self._get_overlap(body.aabb, m_rend.aabb)
                    if overlap[1] == "top":
                        m_rend.y += overlap[0]
                    elif overlap[1] == "bottom":
                        m_rend.y -= overlap[0]
                    elif overlap[1] == "left":
                        m_rend.x -= overlap[0]
                    elif overlap[1] == "right":
                        m_rend.x += overlap[0]

                # Brute-force player against enemies hit check
                if abs(rend.sprite.x - m_rend.sprite.x) < 25:

                    if self._check_overlap(rend.aabb, m_rend.aabb):
                        overlap = self._get_overlap(rend.aabb, m_rend.aabb)
                        if overlap[1] == "top":
                            if abs(rend.x - m_rend.x) > 5:
                                cmd.bump_up = True
                                self._damage_player(ent, life, sfx)
                            else:
                                m_cmd.bump_down = True
                                self._damage_enemy(m_ent, m_life, m_sfx)

                        elif overlap[1] == "bottom":
                            if abs(rend.x - m_rend.x) > 5:
                                cmd.bump_down = True
                                self._damage_player(ent, life, sfx)
                            else:
                                m_cmd.bump_up = True
                                self._damage_enemy(m_ent, m_life, m_sfx)

                        elif overlap[1] == "left":
                            if abs(rend.y - m_rend.y) > 6:
                                cmd.bump_left = True
                                self._damage_player(ent, life, sfx)
                            else:
                                m_cmd.bump_right = True
                                self._damage_enemy(m_ent, m_life, m_sfx)

                        elif overlap[1] == "right":
                            if abs(rend.y - m_rend.y) > 6:
                                cmd.bump_right = True
                                self._damage_player(ent, life, sfx)
                            else:
                                m_cmd.bump_left = True
                                self._damage_enemy(m_ent, m_life, m_sfx)


class AnimationProcessor(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, dt):
        for ent, (rend, anim, vel) in self.world.get_components(Renderable, Animation, Velocity):
            vel_x = vel.x
            vel_y = vel.y

            if abs(vel_x) > abs(vel_y):

                if vel_x > 0:
                    if vel_x > 0.1:
                        rend.image = anim.move_right
                    else:
                        rend.image = anim.idle_right
                elif vel_x < 0:
                    if vel_x < -0.1:
                        rend.image = anim.move_left
                    else:
                        rend.image = anim.idle_left
            else:
                if vel_y > 0:
                    if vel_y > 0.1:
                        rend.image = anim.move_up
                    else:
                        rend.image = anim.idle_up
                elif vel_y < 0:
                    if vel_y < -0.1:
                        rend.image = anim.move_down
                    else:
                        rend.image = anim.idle_down


class AIProcessor(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, dt):
        for ent, (rend, pla) in self.world.get_components(Renderable, Player):

            for ment, (m_ren, m, brain, mcmd), in self.world.get_components(Renderable, Monster, Brain, Command):

                if abs(rend.sprite.x - m_ren.sprite.x) < 100:
                    # Target Player
                    if m.enemy_type == "spider":
                        mcmd.move_up = mcmd.move_down = mcmd.move_left = mcmd.move_right = False
                        if rend.y > m_ren.y + 8:
                            mcmd.move_up = True
                        elif rend.y < m_ren.y - 8:
                            mcmd.move_down = True
                        elif rend.x > m_ren.x - 8:
                            mcmd.move_right = True
                        elif rend.x < m_ren.x + 8:
                            mcmd.move_left = True

                    elif m.enemy_type == "skeleton":

                        if rend.y < m_ren.y:
                            mcmd.move_down = True
                        elif rend.y > m_ren.y:
                            mcmd.move_up = True
                        if rend.x > m_ren.x:
                            mcmd.move_right = True
                        elif rend.x < m_ren.x:
                            mcmd.move_left = True
                else:
                    brain.counter -= 1
                    if brain.counter < 0:
                        mcmd.move_up = mcmd.move_down = mcmd.move_left = mcmd.move_right = False
                        choices = ("move_up", "move_down", "move_left", "move_right", "wait")
                        choice = random.choice(choices)
                        if choice is not "wait":
                            setattr(mcmd, choice, True)
                        brain.counter = brain.cooldown

                if m.enemy_type == "ghost":
                    brain.counter -= 1
                    if brain.counter < 0:
                        mcmd.move_up = mcmd.move_down = mcmd.move_left = mcmd.move_right = False
                        choices = ("move_up", "move_down", "move_left", "move_right")
                        choice = random.choice(choices)
                        setattr(mcmd, choice, True)
                        brain.counter = brain.cooldown


class LifeProcessor(esper.Processor):
    def __init__(self, scene_setter):
        super().__init__()
        self.set_scene = scene_setter

    # def _create_corpse(self, animation, position):
    #     corpse_ent = self.world.create_entity()
    #     corpse_sprite = pyglet.sprite.Sprite(img=animation, x=position[0], y=position[1])
    #     self.world.add_component(corpse_ent, Renderable(sprite=corpse_sprite))
    #     self.world.add_component(corpse_ent, Life(lifespan=200))

    def process(self, dt):
        for ent, (rend, life, sfx) in self.world.get_components(Renderable, Life, SFX):
            if life.lifespan:
                life.lifespan -= 1
                if life.lifespan <= 0:
                    self.world.delete_entity(ent)
                    print("Deleted entity")

            if life.hp <= 0:
                # death_anim = self.world.component_for_entity(ent, Animation).dead
                # self._create_corpse(animation=death_anim, position=rend.sprite.position)
                sfx.death.play()
                if self.world.has_component(ent, Player):
                    time.sleep(1)
                    self.set_scene(scene_class_name="GameOverScene")
                self.world.delete_entity(entity=ent)


class FlashProcessor(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, dt):
        for ent, (rend, flash) in self.world.get_components(Renderable, Flash):
            if flash.count <= 0:
                rend.sprite.color, flash.color = flash.color, rend.sprite.color
                flash.count = flash.frequency
            else:
                flash.count -= 1

            flash.duration -= 1
            if flash.duration < 0:
                rend.sprite.color = (255, 255, 255)
                self.world.remove_component(ent, Flash)


class SceneChangeProcessor(esper.Processor):
    def __init__(self, scene_setter):
        super().__init__()
        self.set_scene = scene_setter

    def process(self, dt):
        for ent, trig in self.world.get_component(SceneTrigger):
            print(trig.target)
            if trig.target == "youwin":
                self.set_scene(scene_class_name="WinnerScene")
            else:
                self.set_scene(scene_class_name="DungeonScene", map_name=trig.target)


class HudProcessor(esper.Processor):
    def __init__(self):
        super().__init__()
        self.hud_grid = pyglet.image.ImageGrid(image=pyglet.resource.image("meter.png"), rows=6, columns=1)
        self.hud_sprite = self.hud_grid[0]
        self.hp_selection = [6, 5, 4, 3, 2, 1, 0, 0]

    def process(self, dt):
        for ent, (life, cam) in self.world.get_components(Life, Camera):

            for h_ent, (hud, rend) in self.world.get_components(Hud, Renderable):
                rend.sprite.position = int(cam.x + 10), int(cam.y + 10)
                try:
                    rend.sprite.image = self.hud_grid[self.hp_selection[life.hp]]
                except IndexError:
                    pass
